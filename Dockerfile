FROM openjdk:8-jre-alpine
ADD target/serviceTwo-0.0.1-SNAPSHOT.jar serviceTwo-0.0.1-SNAPSHOT.jar
EXPOSE 9091
ENTRYPOINT ["java","-agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=n","-Djava.security.egd=file:/dev/./urandom","-Xms2g","-Xmx8g","-jar","serviceTwo-0.0.1-SNAPSHOT.jar"]
