package com.example.serviceTwo;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataBaseConfig {
    final String DB_HOST = System.getenv("DB_HOST") == null ? "localhost"
            : System.getenv("DB_HOST");
    final String DB_PORT = System.getenv("DB_PORT") == null ? "3306"
            : System.getenv("DB_HOST");
    final String DB_USER = System.getenv("DB_USER") == null ? "root"
            : System.getenv("DB_USER");
    final String DB_PASSWORD = System.getenv("DB_PASSWORD") == null ? "root"
            : System.getenv("DB_PASSWORD");

    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder
                .url("jdbc:mariadb://" + DB_HOST + ":" + DB_PORT + "/test");
        dataSourceBuilder.username(DB_USER);
        dataSourceBuilder.password(DB_PASSWORD);
        return dataSourceBuilder.build();
    }

}
