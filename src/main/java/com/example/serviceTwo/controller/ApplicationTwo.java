package com.example.serviceTwo.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.serviceTwo.dao.Note;
import com.example.serviceTwo.dao.NoteRepository;

@RestController
@RequestMapping("/servicetwo")
public class ApplicationTwo {

    final String host = System.getenv("SECOND_SERVICE_HOST");
    final String port = System.getenv("SECOND_SERVICE_PORT");

    @Autowired
    NoteRepository noteRepository;

    @RequestMapping("/products")
    public String home() {
        return "Hello Docker World";
    }

    @GetMapping("/notes/{description}")
    public Note createNote(@PathVariable("description") String description) {
        Note note = new Note();
        long generatedLong = new Random().nextLong();
        Date date = new Date();
        note.setId(generatedLong);
        note.setTitle(description.substring(0, 5) + " ......");
        note.setContent(description);
        note.setCreatedAt(date);
        note.setUpdatedAt(date);
        return noteRepository.save(note);
    }

    @GetMapping("/notes")
    public List<Note> getAllNotes() {
        return noteRepository.findAll();
    }
}